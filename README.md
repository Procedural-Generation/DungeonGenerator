# Dungeon Generator
>I[Jay] designed the python dungeon generator after finding that there weren’t many python procedural level generators that were generic enough for use in any game engine/rendering set up. Those that did exist either weren’t very documented or lacked in features. So this aims to rectify that.
>The generator can create rooms, corridors, mazes and cave systems and any mix of them. It contains a number of tools for generating content, searching through it and path finding along with a number of helper functions to support its use. It’s largely based on ideas in this article with my own bits and bobs thrown in to generalise its usage.
>The generator is designed to be as robust as possible. While this allows you to abuse the various functions to get the result you want it also means that if you feed it junk it’ll spit out junk. It places few restrictions on the order that you need to call generator functions and provides plenty of customisation options. It has few dependencies on other python libraries, using only python’s random library.
>
>   -Jay https://whatjaysaid.wordpress.com/2016/01/15/1228/

## This is not an official egg
What this means is you'll need to copy the '**_dungeonGenerator.py_**' into your project folder (or load it manually to your library folder)

## Using the script
Most of this is covered via the initial release [https://whatjaysaid.wordpress.com/2016/01/15/1228/]; in summery here’s basic usage.

### Generate class object
this creates the generator and defines the map size for use
```py3
import dungeonGenerator
levelSize = [64,32]
d = dungeonGenerator.dungeonGenerator(levelSize[0], levelSize[1])
```
#### Populate map structure - Rooms/corridor route
This generates a structure with a boxier look/feel to it
```py3
d.placeRandomRooms(5, 11, 2, 4, 500)
d.generateCorridors()
d.connectAllRooms(30)
d.pruneDeadends(10)
d.placeWalls()
```
#### Populate map structure - Cave route
This generates a structure with more organic look/feel to it
```py3
d.generateCaves(p = 30, smoothing = 2)
d.removeUnconnectedAreas()
d.mergeUnconnectedAreas()
d.placeWalls()
```
### Populate overlay items
Now that the structure has been generated; we can load overlay items (enter/exit points, traps, foes, treasure, artifact)
```py3
d.findHostableTiles()
d.placeEnteranceExitOverlay()
d.placeRandomOverlays(dungeonGenerator.OVERLAY_ARTIFACT, 6, dungeonGenerator.FLOOR, True)
d.placeRandomOverlays(dungeonGenerator.OVERLAY_TREASURE, 15, dungeonGenerator.FLOOR)
d.placeRandomOverlays(dungeonGenerator.OVERLAY_TREASURE, 10)
d.placeRandomOverlays(dungeonGenerator.OVERLAY_TRAP, 6)
d.placeRandomOverlays(dungeonGenerator.OVERLAY_FOE, 6)
```
Thats it; the map is now full defined!

## Rendering the data
There are multiple solutions to get this as an image

### PIL (Pillow)
First make sure to import the required modules with the generator
```py3
import pygame
import dungeonGenerator
```
Generate you map data; define tile information and load into PyGame
```py3
def overlayText(x,y,background,text):
    bg  = ImageDraw.Draw(background)
    ftp = ImageFont.load_default()
    fnt = ImageFont.truetype("swissek.ttf",12)
    bg.text((x*tileSize, y*tileSize), text, font=fnt, fill=(128,255,0))
    return

def overlayGraphic(x,y,background,foreground):
    im=Image.open(foreground)
    background.paste(im, (x*tileSize, y*tileSize), im)

floorGraphicTiles = [dungeonGenerator.FLOOR, dungeonGenerator.CORRIDOR, dungeonGenerator.CAVE]
overlayTypes = 5
tileSize = 32
new_im = Image.new('RGB', ((levelSize[1]+5)*tileSize, (levelSize[0]+overlayTypes+2)*tileSize))

for x, y, tile in d:
    im = None
    if tile == dungeonGenerator.EMPTY:
        im=Image.open('tile_EMPTY.png')        
    elif tile in floorGraphicTiles:
        im=Image.open('tile_FLOOR.png')
    elif tile == dungeonGenerator.WALL:
        im=Image.open('tile_WALL.png')
    elif tile == dungeonGenerator.DOOR:
        if d.grid[x+1][y] == dungeonGenerator.WALL:
            im=Image.open('tile_DOOR-F.png')
        else:
            im=Image.open('tile_DOOR-S.png')
        im2=Image.open('tile_FLOOR.png')
        new_im.paste(im2, (x*tileSize, y*tileSize))
        new_im.paste(im, (x*tileSize, y*tileSize), im)
        im = None
    if im != None:
        new_im.paste(im, (x*tileSize, y*tileSize))
        
im=Image.open('tile_ORIGIN.png')
new_im.paste(im, (0,0), im)

# load overlay tiles
overlayId = 0
cellBreakdown = 'Cell Id break down'
for key, value in d.overlays.items():
    x = value[0]
    y = value[1]    
    i = ''
    l = value[2].items()
    o = len(l)
    
    # place a the graphics
    for t,v in l:
        f = None
        if t == dungeonGenerator.OVERLAY_ARTIFACT:
            f = 'tile_ARTIFACT.png'
            i += '  {0}-{1}\n'.format(v,'Artifacts')            
        elif t == dungeonGenerator.OVERLAY_TREASURE:
            f = 'tile_TREASURE.png'
            i += '  {0}-{1}\n'.format(v,'Treasure')            
        elif t == dungeonGenerator.OVERLAY_TRAP:
            f = 'tile_TRAP.png'
            i += '  {0}-{1}\n'.format(v,'Trap')            
        elif t == dungeonGenerator.OVERLAY_FOE:
            f = 'tile_FOE.png'
            i += '  {0}-{1}\n'.format(v,'Foe')
        elif t == dungeonGenerator.OVERLAY_EXIT:
            i += '  {0}-{1}\n'.format(v,'Exit')
            f = 'tile_EXIT.png'            
        elif t == dungeonGenerator.OVERLAY_ENTER:
            i += '  {0}-{1}\n'.format(v,'Entrance')
            f = 'tile_ENTER.png'            
        else:
            i += '  {0}-{1}\n'.format(v,'UNKNOWN')
            
        if f != None:
            overlayGraphic(x,y,new_im,f)

    # clear graphic if multiple items
    if o > 1:
        overlayId += 1
        overlayGraphic(x,y,new_im, 'tile_FLOOR.png')
        overlayText(x,y,new_im, "{0}".format(overlayId))
        cellBreakdown += "\n({0})\n{1}".format(overlayId,i)           
    
# Load key data for crowded cells
overlayText(levelSize[1],0,new_im, cellBreakdown)

# load deadend graphics like very last or atleast after the enter/exit
# IDK when these would get used ... but its a demo script
for de in d.deadends:
    im=Image.open('tile_DEADEND.png')
    new_im.paste(im, (de[0] * tileSize, de[1] * tileSize), im)
# Load Graphic key
overlayGraphic(0,levelSize[0]+1,new_im,'tile_ENTER.png')
overlayText(1,levelSize[0]+1,new_im, 'Entrance')
overlayGraphic(0,levelSize[0]+2,new_im,'tile_EXIT.png')
overlayText(1,levelSize[0]+2,new_im, 'Exit')
overlayGraphic(0,levelSize[0]+3,new_im,'tile_TREASURE.png')
overlayText(1,levelSize[0]+3,new_im, 'Treasure')
overlayGraphic(0,levelSize[0]+4,new_im,'tile_TRAP.png')
overlayText(1,levelSize[0]+4,new_im, 'Trap')
overlayGraphic(0,levelSize[0]+5,new_im,'tile_FOE.png')
overlayText(1,levelSize[0]+5,new_im, 'Foe')
overlayGraphic(0,levelSize[0]+6,new_im,'tile_ARTIFACT.png')
overlayText(1,levelSize[0]+6,new_im, 'Artifacts')

# save image
new_im.save("screenshot.jpg")
```

### Blender
First make sure to import the required modules with the generator
```py3
from random import choice
import bge
import dungeonGenerator
```
Generate you map data; then get it over to blender like so
```py3
for x, y, c in d:
    if c == dungeonGenerator.FLOOR:
        ob = scene.addObject(choice(['Floor2', 'Floor3']))
        ob.worldPosition.y = y * spacing
        ob.worldPosition.x = x * spacing
        ob.worldOrientation = [0, 0, choice([0, 1.5708, 3.1415, -1.5708])]
    elif c == dungeonGenerator.CORRIDOR:
        ob = scene.addObject('Floor')
        ob.worldPosition.y = y * spacing
        ob.worldPosition.x = x * spacing
    elif c == dungeonGenerator.DOOR:
        ob = scene.addObject('Door')
        ob.worldPosition.y = y * spacing
        ob.worldPosition.x = x * spacing
    elif c == dungeonGenerator.WALL:
        ob = scene.addObject('Wall1')
        ob.worldPosition.y = y * spacing
        ob.worldPosition.x = x * spacing
        ob.worldOrientation = [0, 0, choice([0, 1.5708, 3.1415, -1.5708])]
```

### PyGame
First make sure to import the required modules with the generator
```py3
import pygame
import dungeonGenerator
```
Generate you map data; define tile information and load into PyGame
```py3
tileSize = 16
levelSize = 51
 
screen = pygame.display.set_mode((levelSize * tileSize, levelSize * tileSize))
spriteSheet = pygame.image.load('testSprites.png').convert()
 
floorRect = pygame.Rect(0, 0, tileSize, tileSize)
floorTile = spriteSheet.subsurface(floorRect)
 
wallRect = pygame.Rect(tileSize*3, 0, tileSize, tileSize)
wallTile = spriteSheet.subsurface(wallRect)
 
facingWallRect = pygame.Rect(tileSize*2, 0, tileSize, tileSize)
facingWallTile = spriteSheet.subsurface(facingWallRect)
 
boxRect = pygame.Rect(tileSize*1, 0, tileSize, tileSize)
boxTile = spriteSheet.subsurface(boxRect)

doorRect = pygame.Rect(tileSize*4, 0, tileSize, tileSize)
doorTile = spriteSheet.subsurface(doorRect)
 
doorSideRect = pygame.Rect(tileSize*5, 0, tileSize, tileSize)
doorSideTile = spriteSheet.subsurface(doorSideRect)

for x, y, tile in d:
    if tile == dungeonGenerator.FLOOR:
        screen.blit(floorTile, (x*tileSize, y*tileSize))
    elif tile == dungeonGenerator.CORRIDOR:
        screen.blit(floorTile, (x*tileSize, y*tileSize))
    elif tile == dungeonGenerator.DOOR:
        # rotate the door tile accordingly
        # no need to check bounds since a door tile will never be against the edge
        if d.grid[x+1][y] == dungeonGenerator.WALL:
            screen.blit(doorTile, (x*tileSize, y*tileSize))
        else:
            screen.blit(doorSideTile, (x*tileSize, y*tileSize))
    elif tile == dungeonGenerator.WALL:
        # if the wall tile is facing us lets render a different one
        if y == levelSize-1 or d.grid[x][y+1] != dungeonGenerator.WALL:
            screen.blit(facingWallTile, (x*tileSize, y*tileSize))
        else:
            screen.blit(wallTile, (x*tileSize, y*tileSize))
 
for de in d.deadends:
    screen.blit(boxTile, (de[0] * tileSize, de[1] * tileSize))
 
pygame.display.flip()
```
# Contributors
* Jay of whatjaysaid.wordpress.com
* GK101 @GK101
* Tukirito @Zero_Cool94

# Credits
### Jay of whatjaysaid.wordpress.com
* initial release
* PyGame rendering
* Blender rendering
* bug corrections

### Tukirito @Zero_Cool94
* testing
* bug corrections
* repository creating

### GK101 @GK101
* Overly extension
* bug corrections
* 